const Router = require('koa-router');

const VERSION = '/api/v1';

module.exports = ({ getBooks, postBooks, patchBooks }) => {
  const router = new Router();

  router.get(`${VERSION}/books`, async (ctx) => {
    ctx.body = await getBooks(ctx);
  });

  router.post(`${VERSION}/book`, async (ctx) => {
    ctx.body = await postBooks(ctx);
  });

  router.patch(`${VERSION}/book/:id`, async (ctx) => {
    ctx.body = await patchBooks(ctx);
  });

  return router;
};
