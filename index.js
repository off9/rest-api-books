const Koa = require('koa');
const koaBody = require('koa-body');
const db = require('./db');
const api = require('./api')(db);
const router = require('./router')(api);

const app = new Koa();

app.use(koaBody());
app.use(router.routes()).use(router.allowedMethods());

app.listen(3000, () => {
  console.log('Start server');
});
