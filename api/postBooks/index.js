const setBooksDB = require('./setBookDB');

module.exports = ({ query }) => {
  async function postBooks(ctx) {
    const bookObj = {
      title: ctx.request.body.title,
      author: ctx.request.body.author,
      description: ctx.request.body.description,
      date: ctx.request.body.date,
      image: ctx.request.body.image,
    };
    const queriString = setBooksDB(bookObj);
    // console.log(queriString);
    // console.log(await query(queriString));
    return { 'Add string': (await query(queriString)).results.affectedRows };
  }
  return postBooks;
};
