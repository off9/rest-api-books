const security = require('../ securityModule');

// function db(query) {
//   console.log(query);
// }

function queryBilderInsert(obj) {
  const values = [];

  if (obj.title) { values.push(`'${obj.title}'`); }
  if (obj.author) { values.push(`'${obj.author}'`); }
  if (obj.description) { values.push(`'${obj.description}'`); }
  if (obj.date) { values.push(obj.date); }
  if (obj.image) { values.push(`'${obj.image}'`); }


  return `INSERT INTO books (title, author, description, date, image) VALUES (${values.join(', ')})`;
}

function setbookDB(filterObj) {
  security(filterObj);

  return queryBilderInsert(filterObj);
}

module.exports = setbookDB;
