function objectCheckForInjection(obj) {
  if (Object.values(obj).some((el) => !(typeof el !== 'string' || typeof el !== 'number' || typeof el !== 'boolean') || /[\;\']/g.test(el))
  ) {
    throw new Error('WARN: Is possible SQL injection');
  }
}
module.exports = objectCheckForInjection;
