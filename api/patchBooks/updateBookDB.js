const security = require('../ securityModule');

function queryBilderUpdate(obj) {
  if (!obj.id) {
    throw new Error('Update ID null');
  }

  const set = [];

  if (obj.title) { set.push(`title = '${obj.title}'`); }
  if (obj.author) { set.push(`author = '${obj.author}'`); }
  if (obj.description) { set.push(`description = '${obj.description}'`); }
  if (obj.date) { set.push(`date = ${obj.date}`); }
  if (obj.image) { set.push(`image = '${obj.image}'`); }


  return `UPDATE books SET ${set.join(', ')} WHERE id = ${obj.id}`;
}


function updateBookDB(bookObj) {
  security(bookObj);

  return queryBilderUpdate(bookObj);
}

module.exports = updateBookDB;
