const updateBookDB = require('./updateBookDB');

module.exports = ({ query }) => {
  async function patchBooks(ctx) {
    const bookObj = {
      id: ctx.params.id,
      title: ctx.request.body.title,
      author: ctx.request.body.author,
      description: ctx.request.body.description,
      date: ctx.request.body.date,
      image: ctx.request.body.image,
    };
    const queriString = updateBookDB(bookObj);
    return { 'Update string': (await query(queriString)).results.affectedRows };
  }
  return patchBooks;
};
