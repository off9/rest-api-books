const security = require('../ securityModule');

function queryBilderSelect(obj) {
  if (obj.id) {
    return `SELECT title, date, description, image, author
            FROM books WHERE books.id = ${obj.id}`;
  }
  const where = [];

  if (obj.titleIncludes) { where.push(`title LIKE '%${obj.titleIncludes}%'`); }
  if (obj.titleExcludes) { where.push(`title NOT LIKE '%${obj.titleExcludes}%'`); }
  if (obj.dateStart) { where.push(`date >= ${obj.dateStart}`); }
  if (obj.dateEnd) { where.push(`date <= ${obj.dateEnd}`); }
  if (obj.authorIncludes) { where.push(`author LIKE '%${obj.authorIncludes}%'`); }
  if (obj.authorExcludes) { where.push(`author NOT LIKE '%${obj.authorExcludes}%'`); }
  if (obj.descriptionIncludes) { where.push(`description LIKE '%${obj.descriptionIncludes}%'`); }
  if (obj.descriptionExcludes) { where.push(`description NOT LIKE '%${obj.descriptionExcludes}%'`); }
  if (obj.image) { where.push(`image=${obj.image}`); }

  const whereString = where.length ? `WHERE ${where.join(' AND ')}` : '';

  return `SELECT title, date, description, image, author 
          FROM books ${whereString}`;
}

function getDataDb(filterObj) {
  security(filterObj);
  const query = queryBilderSelect(filterObj);
  return query;
}


module.exports = getDataDb;
