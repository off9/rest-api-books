module.exports = (setapCashed = new Map([])) => {
  const cashed = setapCashed;
  function check(key) {
    return cashed.has(JSON.stringify(key)) ? cashed.get(JSON.stringify(key)) : false;
  }
  function set(key, data) {
    cashed.set(JSON.stringify(key), data);
  }
  return { check, set };
};
