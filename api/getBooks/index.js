const orderBy = require('lodash/orderBy');
const getQueryString = require('./getQueryString');
const { check, set } = require('./cashedModul')();

module.exports = ({ query }) => {
  function parseSort(options) {
    if (!options) return false;
    return options.split(',').reduce(
      (obj, el) => {
        let iteratees = '';
        let elemSort = '';
        if (el[0] === '-') {
          iteratees = 'desc';
          elemSort = el.slice(1);
        } else {
          elemSort = el;
          iteratees = 'asc';
        }

        return { sort: [...obj.sort, elemSort], desc: [...obj.desc, iteratees] };
      },
      { sort: [], desc: [] },
    );
  }

  async function getBooks(ctx) {
    let data;
    const optionsSort = parseSort(ctx.request.query.sort);
    const optionsFilter = {
      id: ctx.request.query.id,
      titleIncludes: ctx.request.query.title,
      titleExcludes: ctx.request.query.nottitle,
      dateStart: ctx.request.query.datestart,
      dateEnd: ctx.request.query.dateend,
      authorIncludes: ctx.request.query.author,
      authorExcludes: ctx.request.query.notauthor,
      descriptionIncludes: ctx.request.query.description,
      descriptionExcludes: ctx.request.query.notdescription,
      image: ctx.request.query.image,
    };
    const cash = check(optionsFilter);
    if (cash) {
      data = cash;
    } else {
      const booksAll = await getQueryString(optionsFilter);
      data = await query(booksAll);
      set(optionsFilter, data);
    }

    if (optionsSort) {
      return orderBy(data.results, optionsSort.sort, optionsSort.desc);
    }
    return data;
  }
  return getBooks;
};
