const getBooks = require('./getBooks');
const postBooks = require('./postBooks');
const patchBooks = require('./patchBooks');

module.exports = (db) => ({
  getBooks: getBooks(db),
  postBooks: postBooks(db),
  patchBooks: patchBooks(db),
});
