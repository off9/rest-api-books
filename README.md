# Тестовое задание

Недоработано:

1. Нереализована зависимость много ко многим в SQL
2. Отсутствуют тесты
3. Отсутствует документация

=====================

Запросы API:
HOST - хост:порт

**Получить конкретную книгу**

    METHOD: GET
    URL:HOST/api/v1/books

    Get params:
    id - id книги

    exemple:
    HOST/api/v1/books?id=5

**Пулучить все книги**

    METHOD: GET
    URL: HOST/v1/books?

    Get params:
    sort - Параметры для сортировкичерез ',' Для сортировки по убыванию перед параметром идет '-'
    dateend - Фильтрация по дате (до даты)
    datestart - Фильтрация по дате (от даты)
    title - Только книги содержащие в заголовке
    nottitle - Исключить книги содержащие в заголовке
    image - true|false только книзи с рисунком | без него
    description - Только книги содержащие в описании
    notdescription - Исключить книги содержащие в описании
    author - Фильтация по автору
    notauthor - Все книги кроме книг данного автора

    exemple:
    HOST/api/v1/books?sort=-date,title&dateend=17092010&datestart=10092011&title=world&nottitle=noworld&image=true&description=text_desc&notdescription=text_not_desc&author=gamlet&notauthor=notgamlet

**Создать новую книгу**
METHOD: POST
URL: HOST/api/v1/book
Body params:
  
 title - заголовок
author - Автор (в данный момент авторы передаются все в одной строке разбитие на таблицы нет)
description - описание
date - дата в формате (ddmmyyyy)
image - ссылка на картинку

**Редактировать книгу**
METHOD: PATCH
HOST/api/v1/book/{id}

    id - id книги для изменения
    title - заголовок
    author - Автор (в данный момент авторы передаются все в одной строке разбитие на таблицы нет)
    description - описание
    date - дата в формате (ddmmyyyy)
    image - ссылка на картинку
