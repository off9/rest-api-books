const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'testjob',
  password: 'ei7veeChu4bo',
  database: 'test_1',
});

exports.query = (string) => new Promise((res, rej) => {
  connection.query({ sql: string, timeout: 60000 }, (error, results) => {
    if (error) rej(error);
    res({ results });
  });
});
